'use strict'

const Database = use('Database')
const User = use('App/Models/User')

class AuthController {
  async register({ request, response }) {
    const trx = await Database.beginTransaction()

    try {
      const { name, email, password, role, cel, cpf } = request.all()

      const findUser = await User.findBy('email', email)

      if (findUser) {
        return response.status(409).send({ message: 'email já cadastrado' })
      }

      const user = await User.create({ name, email, password, cel, role, cpf }, trx)
      // commita a transaction
      await trx.commit()


      return response.status(201).send({
        message: 'Usuário cadastrado com sucesso!',
        user: user
      })
    } catch (e) {
      await trx.rollback()
      return response.status(400).send({
        error: 'Erro ao realizar cadastro',
        message: e.message
      })
    }
  }



  async login({ request, response, auth }) {

    const { email, password } = request.only(['email', 'password'])

    let data = await auth.withRefreshToken().attempt(email, password);

    const user = await User.findBy('email', email)

    return response.send({ data, user })
  }

  async loginPartner({ request, response, auth }) {

    const { cnpj, password } = request.only(['cnpj', 'password'])

    const userPartner = await Partner.findByOrFail('cnpj', cnpj)
    const userAux = await User.findOrFail(userPartner.user_id)

    let data = await auth.withRefreshToken().attempt(userAux.email, password)

    const user = await User.findByOrFail('email', userAux.email)
    await user.load('partner')

    return response.send({ data, user })
  }

  async forgotPassword({ request, response }) {
    try {

      var { cnpj_email } = request.post()
      var email = cnpj_email
      if (cnpj_email) {
        const partner = await Partner.findBy('cnpj', cnpj_email)
        if (partner) {
          const userPartner = await partner.user().fetch()
          email = userPartner.email
        }
      }

      const user = await User.findBy('email', email)

      if (!user)
        response.status(404).send({ message: 'Email não encontrado.' })

      const resetedInfo = await resetedPassword.create({ email });

      const id = resetedInfo.id;
      const max = 6 - id.toString().length;
      let code = Math.random() * 1000000
      code = code.toString();
      code = code.substr(0, max);
      code = `${id}${code}`

      resetedInfo.code = code;

      await resetedInfo.save();

      await Mail.send('forgotpassword', { code }, (message) => {
        message
          .to(user.email)
          .from('noreplay@rebox.com.br')
          .subject('Código para redefinir sua senha')
      })

      return { message: 'Email enviado com sucesso.', data: resetedInfo }
    }
    catch (error) {
      return error.message
    }
  }
  async changePassword({ request, auth, response }) {
    const user = await auth.getUser()
    user.merge(request.only(['password']))
    await user.save()
    return response.status(200).send({
      message: 'Senha alterada com sucesso'
    })


  }
  async validateCode({ request, response }) {
    const { code, email } = request.only(['code', 'email'])

    const requested = await resetedPassword.query()
      .where('code', code)
      .andWhere('email', email)
      .first();

    if (!requested)
      return response.status(404).send({ message: 'Código e/ou email inválido' })

    const limitTime = moment(requested.created_at).add(10, 'minutes');

    if (moment() > limitTime)
      return response.status(401).send({ message: 'Link expirado.' })

    return requested
  }


  async resetPassword({ auth, request, params, response }) {

    const { password, id } = request.only(['password', 'id']);

    const requested = await resetedPassword.find(id);

    if (!requested)
      return response.status(404).send({ message: 'Código e/ou email inválido' })

    if (requested.status == 0)
      return response.status(404).send({ message: 'Código já utilizado' })

    const user = await User.query()
      .where('email', requested.email)
      .first()

    user.password = password
    await user.save()

    const data = await auth.attempt(user.email, password)

    requested.status = 0;
    await requested.save();

    return { data, user };
  }
}

module.exports = AuthController


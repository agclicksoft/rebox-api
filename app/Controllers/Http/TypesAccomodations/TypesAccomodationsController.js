'use strict'

const TypesAccomodation = use('App/Models/TypesAccomodation')

class TypesAccomodations {

    async index() {
      const results = await TypesAccomodation.query().fetch()
      return results
    }

}

module.exports = TypesAccomodations

'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {

Route.resource('types-accomodations', 'TypesAccomodationsController')

})
.namespace('TypesAccomodations')
.prefix('v1/')

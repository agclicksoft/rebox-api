'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DescriptionAccommodationSchema extends Schema {
  up () {
    this.create('description_accommodations', (table) => {
      table.increments()
      table.integer('accommodation_id').unsigned().references('id').inTable('accommodations')
      table.string('bed_type')
      table.integer('bed_quantity')
      table.integer('adult_quantity')
      table.integer('kids_quantity')
      table.string('bath_private')
      table.timestamps()
    })
  }

  down () {
    this.drop('description_accommodations')
  }
}

module.exports = DescriptionAccommodationSchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GaleryPropretiesSchema extends Schema {
  up () {
    this.create('galery_propreties', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('path').nullable()
      table.string('key').nullable()
      table.string('name').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('galery_propreties')
  }
}

module.exports = GaleryPropretiesSchema

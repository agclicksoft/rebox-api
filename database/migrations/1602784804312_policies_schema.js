'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoliciesSchema extends Schema {
  up () {
    this.create('policies', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('description', 6000)
      table.timestamps()
    })
  }

  down () {
    this.drop('policies')
  }
}

module.exports = PoliciesSchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactsSchema extends Schema {
  up () {
    this.create('contacts', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('occupation')
      table.string('email')
      table.string('phone')
      table.string('more_contacts')
      table.timestamps()
    })
  }

  down () {
    this.drop('contacts')
  }
}

module.exports = ContactsSchema

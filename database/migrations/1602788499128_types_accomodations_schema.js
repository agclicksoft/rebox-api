'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TypesAccomodationsSchema extends Schema {
  up () {
    this.create('types_accomodations', (table) => {
      table.increments()
      table.string('description')
      table.timestamps()
    })
  }

  down () {
    this.drop('types_accomodations')
  }
}

module.exports = TypesAccomodationsSchema

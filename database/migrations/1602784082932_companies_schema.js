'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompaniesSchema extends Schema {
  up () {
    this.create('companies', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('cnpj')
      table.string('state_subscription')
      table.string('company_name')
      table.string('zip_code')
      table.string('address_state')
      table.string('address_city')
      table.string('address_neighborhood')
      table.string('address_street')
      table.string('address_number')
      table.string('address_complement')
      table.timestamps()
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompaniesSchema

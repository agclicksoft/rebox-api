'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AccommodationSchema extends Schema {
  up () {
    this.create('accommodations', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('apartment_name')
      table.string('custom_name')
      table.string('apartment_number')
      table.string('room_umber')
      table.string('living_room_number')
      table.string('bathroom_number')
      table.string('tariff')
      table.string('breakfast')
      table.string('iss')
      table.string('bathroom')
      table.string('convenience')
      table.string('entertainment')
      table.integer('couch_quantity')
      table.integer('guest_quantity')
      table.string('more_beds')
      table.timestamps()
    })
  }

  down () {
    this.drop('accommodations')
  }
}

module.exports = AccommodationSchema

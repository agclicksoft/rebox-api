'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AccommodationSchema extends Schema {
  up () {
    this.create('service_accommodations', (table) => {
      table.increments()
      table.integer('facility_service_id').unsigned().references('id').inTable('facility_services')
      table.string('type')
      table.string('value')
      table.timestamps()
    })
  }

  down () {
    this.drop('accommodations')
  }
}

module.exports = AccommodationSchema

'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PropretiesSchema extends Schema {
  up () {
    this.create('propreties', (table) => {
      table.increments()
      table.string('name')
      table.string('accommodation')
      table.string('address_zip_code')
      table.string('address_state')
      table.string('address_city')
      table.string('address_neighborhood')
      table.string('address_street')
      table.string('address_number')
      table.string('address_complement')
      table.string('phone')
      table.timestamps()
    })
  }

  down () {
    this.drop('propreties')
  }
}

module.exports = PropretiesSchema

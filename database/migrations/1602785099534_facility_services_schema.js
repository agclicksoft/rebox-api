'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacilityServicesSchema extends Schema {
  up () {
    this.create('facility_services', (table) => {
      table.increments()
      table.integer('proprety_id').unsigned().references('id').inTable('propreties')
      table.string('meals')
      table.string('languages')
      table.timestamps()
    })
  }

  down () {
    this.drop('facility_services')
  }
}

module.exports = FacilityServicesSchema

'use strict'

/*
|--------------------------------------------------------------------------
| TypesAcommodationSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const TypesAccomodation = use('App/Models/TypesAccomodation')
class TypesAcommodationSeeder {
  async run () {
    const types = [
      'Hotel',
      'Pousada',
      'Casa de Temporada',
      'Apartamento',
      'Cama e Café',
      'Hotel-Fazenda',
      'Hospedagem Domiciliar',
      'Hotel-Fazenda Rural',
      'Chalé',
      'Vila',
      'Chalé alpino',
      'Albergue',
      'Motel Americano',
      'Pousada Campestre',
      'Hotel Cápsula',
      'Apart-Hotel',
      'Resort',
      'Parque Turístico',
      'Camping',
      'Camping de Luxo',
      'Riad',
      'Ryokan Japonês',
      'Motel',
      'Barco',
      'Hotel Hipereconômico'
    ]
    for (const type of types) {
      await TypesAccomodation.create({
        description: type
      })
    }
  }
}

module.exports = TypesAcommodationSeeder
